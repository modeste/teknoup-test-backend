/**
 * Custom configuration
 * (sails.config.autoreload)
 *
 * One-off settings specific to your application.
 *
 * For more information on custom configuration, visit:
 * https://sailsjs.com/config/custom
 */

module.exports.autoreload = {

  /***************************************************************************
  *                                                                          *
  * For app auto reload.    *
  *                                                                          *
  ***************************************************************************/

  active: true,
  usePolling: false,
  dirs: [
    "api/models",
    "api/controllers",
    "api/policies",
    "api/helpers",
  ],
  ignored: [
    // Ignore all files with .ts extension
    "**.ts"
  ]

};
