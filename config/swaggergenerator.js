module.exports['swagger-generator'] = {
    disabled: false,
    swaggerJsonPath: './swagger/swagger.json',
    swagger: {
        openapi: '3.0.0',
        info: {
            title: 'Tekno UP XP API',
            description: 'This is the Tekno UP XP API documentation',
            termsOfService: 'https://teknoup.com/fr/',
            contact: {name: 'Modeste Gougbedji', url: 'https://teknoup.com/fr/', email: 'gougbedjimodeste@gmail.com'},
            license: {name: 'Tekno UP', url: 'https://teknoup.com/fr/'},
            version: '1.0.0'
        },
        servers: [
            { url: 'http://localhost:7000/' }
        ],
        externalDocs: {url: 'https://teknoup.com/fr/'}
    },
    defaults: {
        responses: {
            '200': { description: 'The requested resource' },
            '404': { description: 'Resource not found' },
            '500': { description: 'Internal server error' }
        }
    },
    excludeDeprecatedPutBlueprintRoutes: true,
    includeRoute: function(routeInfo) { return true; },
};