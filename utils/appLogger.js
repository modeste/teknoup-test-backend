module.exports = {

  appLogger: {
    info: (message, ...metadata) => {
      return sails.log.warn(message, ...metadata);
    },
    error: (message, ...metadata) => {
      return sails.log.error(message, ...metadata);
    },

  }
};

