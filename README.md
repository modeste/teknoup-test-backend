# teknoup-test-backend
## Project setup

Intall Package 
### `npm install`

Start application with the sailsJS command
### `sails lift`

Start application with the sailsJS command and specify port
### `sails lift --port 7000`

Start application with the sailsJS command and in staging 
### `sails lift --staging`

Start application with the sailsJS command and in production 
### `sails lift --prod`


