const fs = require("fs");
module.exports = {

    friendlyName: '',
    description: '',
    
    inputs: {
        filename: {
            type: 'string'
        },
    },

    exits: {

    },

    fn: async function (inputs, exits) {
        var uploadLocation = require('path').resolve(sails.config.appPath, 'assets/downloads') + "/" + inputs.filename
        var tempLocation = require('path').resolve(sails.config.appPath, '.tmp/public/downloads') + "/" + inputs.filename

        //Copy the file to the temp folder so that it becomes available immediately
        fs.createReadStream(uploadLocation).pipe(fs.createWriteStream(tempLocation));
        // TODO

        return exits.success(true)
    }
};

