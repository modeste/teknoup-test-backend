module.exports = {
  friendlyName: 'Delete Annonce',
  description: '',
  inputs: {
    annonceId: {
      type: 'string',
      required: true,
    },
  },

  exits: {
    badRequest: {
      description: 'Something is missing',
      responseType: 'badRequest'
    },
    notFound: {
      statusCode: 404,
      responseType: 'notFound'
    },
    success: {
      description: 'Success',
      responseType: '',
      statusCode: 200
    }
  },

  fn: async function (inputs, exits) {
    sails.config.globals.appLogger.info("-start annonce", inputs);

    let annonce = await Annonce.findOne({ id: inputs.annonceId })
      .intercept((err) => {
        throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
      });

    if (!annonce) {
      throw { badRequest: { status: "error", data: {}, errors: [{ code: "annonceNotFound", message: "L'annonce n'existe pas", field: [{}] }] } };
    }

    await Annonce.archive({ id: inputs.annonceId })
      .intercept((err) => {
          throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
      })

    sails.config.globals.appLogger.info("-end annonce: success", inputs.annonceId)
    return exits.success({ status: "success", data: { annonceId: inputs.annonceId } })
  }
}
