let moment = require('moment');

module.exports = async function (req, res) {
     
    req.file('uri').upload({
        dirname: require('path').resolve(sails.config.appPath, 'assets/downloads')
    }, async function (err, uploadedFiles) {
        // array for all files uploaded
        if (err) {
            return res.status(500).send(err)
        } else if (uploadedFiles.length === 0) {
            //console.log("this user have not file")
        } else {
            //console.log("this user have a very file")
            // for use req.upload 
            //console.log("uploadedFiles", uploadedFiles)

            let fileNameSync = ""
            uploadedFiles.forEach(file => { 
                fileNameSync = file.fd.split('/').reverse()[0]

                let c = file.fd.split('/')
                file.uploadedName = c[c.length - 1]
                file.fd = file.uploadedName
            })

            await sails.helpers.uploadSync.with({ filename: fileNameSync }) // REQUIRED for SYNC auto file

            return res.status(200).send({ status: "success", data: uploadedFiles, errors: [{ code: "", message: "", field: [{}] }] })
        }
    })
}