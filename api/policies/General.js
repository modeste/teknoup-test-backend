module.exports = async (req, res, next) => {
  if (req.method === "DELETE") {
    return res.status(401).json({ code: 'CannotApplyTypeRequest', message: 'Cannot make a DELETE request for all endpoints' })
  }

  next()
}
